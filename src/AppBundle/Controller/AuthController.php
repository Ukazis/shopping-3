<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Subject;
use AppBundle\Entity\SubjectPage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Model\Login;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/auth")
 */
class AuthController extends Controller {

    /**
     * @Route("/login", name="login")
     * @Method("POST")
     */
    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->redirect('/');
    }


    /**
     * @Route("/register", name="register_")
     * @Method("POST")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder) {
        $username = $request->request->get('login');
        $password = $request->request->get('password');

        $em = $this->getDoctrine()->getManager();
        $userRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\User::class);

        $userAlreadyExists = $userRepository->findByUsername($username);

        if($userAlreadyExists) {
            return $this->redirect('/register');
        }

        $client = new User();

        $encoded = $encoder->encodePassword($client, $password);
        $client->setUsername($username);
        $client->setPassword($encoded);

        $em->persist($client);
        $em->flush();

        return $this->redirect('/');
    }
}

