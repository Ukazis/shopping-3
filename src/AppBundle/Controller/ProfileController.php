<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Subject;
use AppBundle\Entity\SubjectPage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Model\Login;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/profile")
 */
class ProfileController extends Controller {

    /** @Route("/") */
    public function showProfile() {
        return $this->render('profile/profile.html.twig');
    }

    /** @Route("/adminPanel") */
    public function showAdminPanel() {
        $userRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\User::class);
        $users = $userRepository->findAll();

        return $this->render('profile/profile.adminPanel.html.twig', ['users' => $users]);
    }
}

