<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Subject;
use AppBundle\Entity\SubjectPage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Model\Login;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


/**
 * @Route("/subject")
 */
class SubjectController extends Controller {

    /**
     * @Route("/addPage")
     * @Method("POST")
     */
    public function addSubjectPageAction(Request $request) {
        $subName = $request->request->get('subject');

        $em = $this->getDoctrine()->getManager();
        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);

        $user = $this->getUser();

        $subject = $subjectRepository->findOneBy([
            'name'=>$subName, 'user'=>$user
        ]);

        $subject->setNumberOfPages($subject->getNumberOfPages() + 1);
        $subject = $em->merge($subject);

        $subjectPage = new SubjectPage();
        $subjectPage->setPageNumber($subject->getNumberOfPages());
        $subjectPage->setSubject($subject);
        $subjectPage->setTitle("page ".$subject->getNumberOfPages());


        $em->persist($subjectPage);
        $em->flush();

        $jsonResponse = json_encode([
            'title'=>$subjectPage->getTitle(),
            'subject'=>$subject->getName(),
            'page'=>$subjectPage->getPageNumber()
        ]);

        return new Response($jsonResponse);
    }

    /**
     * @Route("/delete")
     * @Method("POST")
     */
    public function deleteSubjectAction(Request $request) {
        $subName = $request->request->get('subject');

        $em = $this->getDoctrine()->getManager();
        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);

        $user = $this->getUser();

        $subject = $subjectRepository->findOneBy([
            'name'=>$subName, 'user'=>$user
        ]);

        $em->remove($subject);
        $em->flush();

        return new Response();
    }


    /**
     * @Route("/newSubject")
     * @Method("POST")
     */
    public function addNewSubject(Request $request) {
        $name = $request->request->get('name');

        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);

        $subject = new Subject();
        $subject->setName($name);
        $subject->setPageSufix($name);

        $userRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\User::class);
        $user = $this->getUser();
        $subject->setUser($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($subject);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/deletePage")
     * @Method("POST")
     */
    public function deleteSubjectPageAction(Request $request) {
        $subName = $request->request->get('subject');
        $pageNr = $request->request->get('page');

        $em = $this->getDoctrine()->getManager();
        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);
        $subjectPageRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\SubjectPage::class);

        $user = $this->getUser();

        $subject = $subjectRepository->findOneBy([
            'name'=>$subName, 'user'=>$user
        ]);

        $page = $subjectPageRepository->findOneBy([
            'subject'=>$subject, 'pageNumber'=>$pageNr
        ]);

        $em->remove($page);
        $em->flush();

        return new Response();
    }

    /** @Route("/{subName}") */
    public function subjectAction($subName) {
        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);
        $user = $this->getUser();

        $subject = $subjectRepository->findOneBy([
            'name'=>$subName, 'user'=>$user
        ]);

        return $this->render("subject/subject.html.twig", ['subject'=>$subject]);
    }


    /**
     * @Route("/{subName}/{pageNr}", requirements={"pageNr": "\d+"})
     */
    public function subjectPageAction($subName, $pageNr) {
        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);
        $pageRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\SubjectPage::class);

        $user = $this->getUser();

        $subject = $subjectRepository->findOneBy([
            'name'=>$subName, 'user'=>$user
        ]);

        $page = $pageRepository->findOneBy([
            'subject'=>$subject, 'pageNumber'=>$pageNr
        ]);

        return $this->render('subject/subjectPage.html.twig', ['subjectPage'=>$page]);
    }
}

