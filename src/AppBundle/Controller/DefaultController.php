<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Subject;
use AppBundle\Entity\SubjectPage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Model\Login;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * @Route("/", name="index")
     * @Route("/register", name="register")
     */
    public function indexAction(Request $request) {
        $sourceName = $request->get("_route");
        return $this->render($sourceName.'.html.twig');
    }


    public function headerAction() {
        $username = null;
        if(isset($_SESSION['username'])) {
            $username = $_SESSION['username'];
        }

        return $this->render('templates/header.html.twig',['username' => $username]);
    }


    public function sideBarAction() {
        $subjectRepository = $this->getDoctrine()->getRepository(\AppBundle\Entity\Subject::class);

        $user = $this->getUser();

        $subjects = $subjectRepository->findByUser($user);
        return $this->render('templates/sideBar.html.twig',['subjects'=>$subjects]);
    }
}

