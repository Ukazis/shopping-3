<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\User;

/**
 * @ORM\Entity
 * @ORM\Table( name="subjects",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="user_subject_unique", columns={"name","user"})}
 * )
 */
class Subject {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $subjectId;

    /** @ORM\Column(type="string", length=30) */
    private $name;

    /** @ORM\Column(type="integer") */
    private $numberOfPages = 0;

    /** @ORM\Column(type="string") */
    private $pageSufix;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="subjects")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /** @ORM\OneToMany(targetEntity="SubjectPage", mappedBy="subject") */
    private $pages;


    public function __construct() {
        $this->pages = new ArrayCollection();
    }


    /** @return integer */
    public function getSubjectId() {
        return $this->subjectId;
    }

    /** @return string */
    public function getName() {
        return $this->name;
    }

    /** @return integer */
    public function getNumberOfPages() {
        return $this->numberOfPages;
    }

    /** @return mixed */
    public function getPageSufix() {
        return $this->pageSufix;
    }

    /** @return \AppBundle\Entity\User */
    public function getUser() {
        return $this->user;
    }

    /** @return ArrayCollection */
    public function getPages() {
        return $this->pages;
    }


    /** @param integer $subjectId */
    public function setSubjectId($subjectId) {
        $this->subjectId = $subjectId;
    }

    /** @param string $name */
    public function setName($name) {
        $this->name = $name;
    }

    /** @param integer $numberOfPages */
    public function setNumberOfPages($numberOfPages) {
        $this->numberOfPages = $numberOfPages;
    }

    /** @param mixed $pageSufix */
    public function setPageSufix($pageSufix) {
        $this->pageSufix = $pageSufix;
    }

    /** @param \AppBundle\Entity\User $user */
    public function setUser($user) {
        $this->user = $user;
    }
}