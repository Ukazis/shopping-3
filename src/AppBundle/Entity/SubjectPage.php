<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity  @ORM\Table(name="subjectPages") */
class SubjectPage {

    /** @ORM\Column(type="string", length=30) */
    private $title;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $pageNumber;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Subject", inversedBy="pages")
     * @ORM\JoinColumn(name="subject", referencedColumnName="subject_id", onDelete="cascade")
     */
    private $subject;

    /** @ORM\Column(type="string", length=30, nullable=true) */
    private $pdfSource;


    /** @return mixed */
    public function getTitle() {
        return $this->title;
    }

    /** @return mixed */
    public function getPageNumber() {
        return $this->pageNumber;
    }

    /** @return Subject */
    public function getSubject() {
        return $this->subject;
    }

    /** @return string */
    public function getPdfSource() {
        return $this->pdfSource;
    }


    /** @param mixed $title */
    public function setTitle($title) {
        $this->title = $title;
    }

    /** @param mixed $pageNumber */
    public function setPageNumber($pageNumber) {
        $this->pageNumber = $pageNumber;
    }

    /** @param Subject $subject */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /** @param string $pdfSource */
    public function setPdfSource($pdfSource) {
        $this->pdfSource = $pdfSource;
    }
}