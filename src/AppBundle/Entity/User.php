<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/** @ORM\Entity  @ORM\Table(name="users") */
class User implements UserInterface, \Serializable {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="string", length=30, unique=true) */
    private $username;

    /** @ORM\Column(type="string", length=60) */
    private $password;

    /** @ORM\Column(type="array") */
    private $roles = ["ROLE_USER"];

    /** @ORM\OneToMany(targetEntity="Subject", mappedBy="user") */
    private $subjects;

    public function __construct() {
        $this->subjects = new ArrayCollection();
    }


    /** @return integer */
    public function getId() {
        return $this->id;
    }

    /** @return string */
    public function getUsername() {
        return $this->username;
    }

    /** @return string */
    public function getPassword() {
        return $this->password;
    }

    /** @return ArrayCollection */
    public function getSubjects() {
        return $this->subjects;
    }

    /** @return array */
    public function getRoles() {
        return $this->roles;
    }

    /** return string */
    public function getSalt() {
        return null;
    }


    /** @param $username */
    public function setUsername($username) {
        $this->username = $username;
    }

    /** @param $password */
    public function setPassword($password) {
        $this->password = $password;
    }


    public function grantRole($role) {
        if(!in_array($role, $this->roles, true)){
            $this->roles[] = $role;
        }
    }

    public function takeAwayRole($role) {
        $this->roles = array_diff($this->roles, $role);
    }


    public function eraseCredentials() {}

    public function serialize() {
        return serialize([
            $this->id, $this->username, $this->password
        ]);
    }

    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->password
        ) = unserialize($serialized);
    }
}