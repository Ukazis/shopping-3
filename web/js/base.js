var colorIndex = 0;

function changeBGColor() {
    var colors = ['#636363', '#6b9311', '#8959A8', 'yellow'];
    colorIndex = (colorIndex+1)%colors.length;

    document.body.style.backgroundColor = colors[colorIndex];
}

function deleteSubject(event, subjectName) {
    target = $(event.target);

    html = "<div id='dialog' title='delete subject'>"+
        "<p>Are you sure you want delete subject "+subjectName+"</p>"+
        "</div>";

    $(html).dialog({
       resizable: false,
       height: "auto",
       width: 400,
       buttons: {
           "delete": function() {
               $.ajax({
                   method: "POST",
                   url: "/subject/delete",
                   data: { subject: subjectName }
               }).done(function() {
                    target.closest('.panel-success').remove();
               });

               $( this ).dialog( "close" );
           },
           'Cancel': function() {
               $( this ).dialog( "close" );
           }
       }
    });

    event.stopPropagation();            // Do not expand subject in sidePanel
}

function deleteSubjectPage(event, subject, page) {
    target = $(event.target);

    $.ajax({
        method: "POST",
        url: "/subject/deletePage",
        data: { subject: subject, page: page}
    }).done(function() {
        target.closest('.list-group-item').remove();
    });

    event.stopPropagation();            // Do not expand subject in sidePanel
}

function addPage(event, subjectName) {
    target = $(event.target);

    $.ajax({
        method: "POST",
        url: "/subject/addPage",
        dataType: "json",
        data: { subject: subjectName }
    }).done(function(data) {
        target.closest('.panel-success').find('.list-group').append(
            '<li class="list-group-item">' +
                ' <a href="/subject/'+data.subject+'/'+data.page+'">'+data.title+'</a>' +
                ' <a class="removePageButton" title="remove subject" onclick="deleteSubjectPage(event,\''+data.subject+'\',\''+data.page+'\')"><span class="glyphicon glyphicon-remove-sign"></span>  </a>'+
            '</li>'
        );
    });

    event.stopPropagation();
}


$(document).ready(function () {
    $(".panel-success").on({
        mouseenter: function () {
            $(this).children().find('a').css("display", "block");
        },
        mouseleave: function () {
            $(this).children().find('a').css("display", "none")
        }
    }, '.panel-heading');

    $(".list-group").on({
        mouseenter: function () {
            $(this).find('.removePageButton').css("display", "block");
        },
        mouseleave: function () {
            $(this).find('.removePageButton').css("display", "none");
        }
    }, '.list-group-item');
});