<?php

namespace Tests\AppBundle\Controller;

use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShopControllerTest extends WebTestCase {

    public function testIndexResponse() {
        // Arrange
        $client = static::createClient();

        // Act
        $crawler = $client->request('GET', '/shop');

        //Assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}